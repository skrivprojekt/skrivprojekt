\documentclass[a4paper, 12pt, titlepage, swedish, twopage]{book}
\usepackage[utf8]{inputenc}   % Accept european-encoded (latin1) characters.
%\usepackage{a4wide}
\usepackage[a4paper]{geometry}
\usepackage[swedish]{babel}     % For Swedish reports
\usepackage{bookman}
\usepackage{latexsym}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{hyperref} 

\input{cc_beamer}

\newcommand{\stycke}{
\begin{center}
$\Diamond \hspace{1cm} \Diamond \hspace{1cm} \Diamond$
\end{center}\noindent}

%\newcommand{\Stycke}{
%\vspace{\baselineskip}\noindent}
\newcommand{\Stycke}{
\begin{center}
*
\end{center}\noindent}

\newcommand{\tidengick}{
\begin{center}
$\bigtriangleup \hspace{10pt} \odot \hspace{10pt} \bigtriangledown$
\end{center}}

\newcommand{\finis}{
\thispagestyle{empty}
\begin{center}
$\mathfrak{FINIS}$
\end{center}}

\newlength{\saveparindent}
\newlength{\saveparskip}

\newenvironment{ramtext}
{
\setlength{\saveparindent}{\parindent}
\setlength{\saveparskip}{\parskip}
\setlength{\parindent}{0pt}
\setlength{\parskip}{0.618\baselineskip}
\fontfamily{ppl}
\itshape
}{
\normalsize
\setlength{\parindent}{\saveparindent}
\setlength{\parskip}{\saveparskip}
}

\begin{document}
    % Titelsida:

    \vspace*{\stretch{89}}
    \begin{center}
        \noindent \Huge \textbf{Infusionsdryckerna och jag}
    \end{center}
    \vspace{\stretch{144}}
    \begin{center}
        \noindent \large \textbf{Ida-Sofia Skyman}
    \end{center}
    \thispagestyle{empty}
    \newpage

    \vspace*{\fill}
    \footnotesize{
    \noindent \MakeUppercase{En Bok}\\
    \noindent \MakeUppercase{Ida-Sofia Skyman}\\~\\
    \noindent \textcopyright~ \MakeUppercase{Ida-Sofia Skyman}, 2011 \\~\\
    \noindent
    \CcGroupBySa{1}{1ex}\\
    \CcNote{\CcLongnameBySa}\\
    Licenstexten finns tillgänglig på \\
    \texttt{\url{http://creativecommons.org/licenses/by-sa/2.5/se/}} \\
    eller genom att skriva till \\
    \indent Creative Commons \\
    \indent 171 Second St, Suite 300 \\
    \indent San Francisco, CA 94105 USA \\
    \\
    Texten är typsatt i \LaTeX\enspace med hjälp av öppen källkod. 
    \\
    Senast uppdaterad den \today.
    }
    \normalsize
    \thispagestyle{empty}
    \newpage

\subsection*{Svart blod}
''En kaffe tack!'' ''Svart?'' ''Svart!''

\emph{Svart! Som min sargade själ}, vill melodramatikern i mig säga, men jag inser att det knappast kommer falla i god jord. Istället ler jag mot kassörskan, som häller upp kaffet i en pappmugg. Det är ett mycket utstuderat leende -- matt och längtansfullt, tänkt att på samma gång uttrycka utmattning, lidande, ett vänt hopp om en snar frälsning, samt en insikt i det patetiska i min situation. Jag betalar och får kaffet i hand. Med ens ändras leendet, subtilt men otvetydigt blir det lite bredare -- mer uttalat. Utmattningen finns kvar, liksom längtan, men nu med en nyans av iver. Hoppet har fått ge vika för en uppriktig tacksamhet. Tonen av ironisk självmedvetenhet finns fortfarande där. ''Tack!'' sen en paus. Leendet spricker upp ännu något mer: ''Ha det fint!'' ''Detsamma!'' svarar kassörskan. Jag tar några steg bort från kiosken. Stannar. Tar andäktigt en smutt på kaffet. Vandrar vidare. Morgonen därpå upprepas skådespelet.

Låt mig klargöra en sak: kaffe är svart. Kaffemedmjölk är förvisso en dryck med vissa förtjänster, men det är en dryck för andra tillfällen och är \emph{inte} kaffe. Kaffe skall inte heller sötas. Kaffe med socker kan jag lättare acceptera som kaffe, men det är odrickbart. Ett slöseri med gott kaffe. En skymf mot kaffeböndernas idoga arbete med att locka bönorna ur den rika myllan, fjärran från våra tempererade breddgrader. 

Jag är inte så dogmatisk, att jag, som somliga gör, anser att den som inte kan erkänna kaffets rika karaktär, sådan den framstår i dess nakna prakt, bör hålla sig borta. Men jag har heller vare sig förståelse eller tålamod med dem som envisas med att kalla sig kaffedrickare, trots att det inte är kaffe de dricker -- kaffe är svart.

Kaffet är en mörk och het spegel -- den melankoliska själens främsta avbildare. Morgonmackans vän. Fabriksgolvets sociala origo. Ett värm\-ande handslag under en vandringsfärd genom vildmarken, med utsikt över nejden i den daggvåta gryningen. Vederkvickare. Inspiratör. Livskamrat.

När magkatarren för första gången drabbade mig kom den alltså inte bara som ett slag i min fysiska mage, utan även i mitt egos. Rakt i min självbilds teatraliska och smått pompösa mellangärde. Vem var jag nu, utan den där koppen i min hand? Utan den lekfullt ångade koppen -- en halv människa. Ett spöke.

En av mina mer minnesvärda bedrifter på universitetet, i mitt tycke hursomhelst, var att jag lyckades bli först om att spilla kaffe på ett helt nyligen avtäckt golv. Det låter måhända banalt, men det var inte vilket golv som helst. Golvet var designat av en konstnär, finansierat av Statens konstråd. Det var för att bli ett med detta som dropparna föll ur min morgonkopp, inte för att möta något vanligt salsgolv. Efter mig lämnade jag ett spår av bruna blommor. Jag är lika viss om att jag var den förste, som om att jag inte var den siste att spilla; andra har följt mitt exempel. Mitt spår. Men nu då? Vem vill följa ett spöke?

\subsection*{Teet på eftermiddagen}
Till min räddning: teet. Mitt förhållande till te är långt ifrån stormfritt. När jag var barn och mina föräldrar bjöd hem sina kamrater på kvällsaktiviteter grundlades en tidig avsmak för lukten av te. ''Det smakar som te luktar!'' kunde jag utbrista om något föreföll mig särskilt oätligt.

Första gången jag frivilligt drack te var även det en lustig episod. Ett nervöst besök hos en söt flicka. ''Kaffe?'' ''Nej tack!'' ''Jos?'' ''Nej, det går bra.'' ''Te..?'' ''Ja\ldots varför inte?'' 

Man kan ju inte säga nej hela livet. 

Vatten kokades. Te bryggdes. Två ungdomar satte sig på varsin sida om köksbordet. Koppen togs i hand. Teet inmundigades. ''Jag tycker egentligen inte om te,'' säger min värdinna plötsligt. ''Inte jag heller,'' tvingas jag medge, aningen förläget.

Teets estetik är förstås en helt annan än kaffets. Där kaffet kommer med koppen endast, får teentusiasten en hel värld av attiraljer att förhålla sig till: kannan, koppen, silen, tillbehören\ldots Lägg till detta själva bryggandets mysterier. De finns säkert de som lägger lika mycket möda på sitt kaffe som teälskaren på sitt te, men de är garanterat en pittoresk minoritet i jämförelse. Hur länge skall teet dra? Beror förstås på sorten, men även på vattnets hårdhet. Skall man låta tebladen flyta fritt eller bör man använda en behållare? Och glöm för fan inte att värma kannan!

I England, tekulturens högborg, om man blir inbjuden till eftermiddagste -- afternoon tea -- är det dessutom viktigt att förstå att man kommer bjudas på en måltid. Till drycken te hör traditionellt en lång procession av tilltugg, lika viktiga som drycken själv. Rostat bröd med marmelad, scones och sandwhiches; listan kan göras betydligt längre, men har man koll åtminstone på dessa tre, är det min uppfattning att man redan kan komma långt.

Te bör egentligen inte sötas, men det är acceptabelt i de flesta delar av världen att lägga en sockerbit eller två i koppen, alternativt ta en sked honung. I Ryssland är det senare till och med normen. Frågan om huruvida man skall ha mjölk i teet är heller inte på långa vägar lika kontroversiell som för kaffet. Visst finns det fanatiker -- missledda av vanans makt kan man tänka sig -- som benhårt hävdar att te med mjölk inte är te, att det är en styggelse. Dessa förtappade själar har fel. Te kan med fördel drickas med mjölk, även om somliga tesorter, såsom Earl Grey och de flesta gröna varianter, gör sig bäst utan. 

I vilken ordning man ska hälla te och mjölk i koppen däremot, är en stor tvistefråga.

\subsection*{Förebilderna}
Jag får erkänna att jag gärna häller min mjölk i teet. Det är rogivande att se de komplexa formerna framträda ur djupen. Virvlande spiraler av vitt mot rödockra som dansar i koppen till takten av ett rogivande omrörande med skeden. Hypnotiskt.

Till min stora bitterhet är detta allmänt ansett som fel tillvägagångs\-sätt\footnote{''allmänt'' ska här läsas som ''allmänt bland de som någorlunda begriper sig på teets mysterier''; vad en allmännare allmänhet anser är mig helt egalt}. Denna obehagliga överraskning kom till mig direkt från en av mina litterära husgudar, en man vars uttalanden om te bör tas på största allvar: Douglas Adams.

Faktum är att de flesta auktoriteter är rörande överens med Adams, om att mjölken kommer bäst till sin rätt om man häller den i koppen innan man serverar själva teet. Det blandas bättre, och risken är mindre att mjölken klumpar sig till följd av att den skållats\footnote{traditionen lär dock ha uppstått för att minska risken för att kalla keramikkoppar skulle krakelera, då den heta drycken hälldes i dem, innan det beständigare porslinet fanns i vart hushåll}. Är jag därmed dömd att aldrig mer få uppleva skönheten i mjölkens piruetter?
Nej! Till min räddning, en annan litterär husgud: George Orwell.

Douglas Adams var inte den förste, och knappast heller den siste, att nedteckna sina tepreferenser. Ett drygt halvsekel tidigare, mot bakgrund av andra världskrigets teransonering, satte britten Eric Blair, bättre känd som George Orwell, sin penna mot pappret. Han författade en essä, sina landsmän till gagn, om hur man bäst borde njuta sin knappa ranson, på det att man därigenom kunde hålla civilisationens gnista vid liv under de svåra åren. I sin ingående essä argumenterar Orwell för ståndpunkten att mjölken skall hällas i teet. Att han ansåg att det var riktigt är nog för mig att våga gå emot befintlig sed, åtminstone ibland.
 
Men stopp ett tag här nu! Allt detta tal om mjölk; var finns det andra lägret? Det är, som beskrivet ovan, inte nödvändigt att dricka sitt te med mjölk. Själv alternerar jag gärna; faktum är att jag vardagligen oftare dricker teet utan mjölk än med. Även här är jag trygg i förvissningen att jag är i gott sällskap: kommendör Jean-Luc Picards rakryggade integritet är förebilden jag litar till, i detta fall som i så många andra.

Även spöket har sina förebilder, en av dem är även hon kapten i stjärnflottan. Kommendör Kathryn Janeway, befälhavare ombord på USS Voyager, är en inbiten kaffefantast. När man hör henne säga med lidande röst ''Coffee. Black.''  till skeppets replikatorer är igenkännings\-faktorn total. Dessa stunder är också några av få ljusglimtar för den annars platta och moralfloskelstinna karaktären i en medioker TV-serie.

Bättre helhetsintryck finner man i den tecknade världen. I ett avsnitt av Futurama når Fry kaffenirvana efter att ha inmundigat hundra släta koppar. Den bedriften har vunnit honom en plats i mitt hjärta, men allra mest ser jag upp till Wally, den arbetsskygge kollegan till Dilbert i serien med samma namn. För Wally är kaffet ett självändamål; han och koppen är lika oskiljaktiga som jag och den ångande bägaren i mina innerligaste drömbilder. Den är en del av honom, just som jag ville att den skulle vara en del av mig.

\vspace{-2pt}
\subsection*{Slutord}
Jag vill ogärna kalla mig fåfäng, men jag erkänner beredvilligt att för mig har kaffekoppen blivit en accessoar. I mitt yrke markerar den min särställning gentemot eleverna; över kanten på min robusta keramik ser jag ner på deras engångsmuggar i plast eller papp. Den skänker alla eftertänksamma poser ett extra intellektuellt skimmer, något jag tacksamt tar emot då min syn är allt för god för att glasögon skall vara något att tänka på. Kaffekoppen är vidare ett oundgängligt verktyg vid möten: den håller nervösa händer på plats, och är det perfekta redskapet för att göra de pinsamma tystnaderna mindre påtagliga.

Insikten att jag inte längre kunde dricka de mängder kaffe som krävs för att hålla uppe bilden av den förvirrade intellektuelle var smärtsam, men lärorik. När vardagen kollapsar och man tvingas reflektera över sina levnadsmönster ser man saker i ett nytt ljus. Ibland skänker detta insikter; man ser essensen bakom ytan. Vad jag såg var att den centrala symbolen inte var kaffet. Huruvida man föredrar en infusion av te, kaffe, kakao eller örter spelar mindre roll än kärlet varur man dricker. Jag håller därför mitt standar högt, dess dimslöjor en fana bakom mig när jag tågar mot framtiden: den vänligt ångande koppen.
\end{document}
