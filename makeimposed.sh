#/bin/bash
# 2 a5 pages per a4: (print with -o sides=two-sided-short-edge)
pdftops boken.pdf -|psbook -s12|psnup -Pa5 -pa5 -2|psresize -Pa5 -pa4|ps2pdf -sPAPERSIZE=a4 -dEmbedAllFonts=true -dOptimized=true ->boken_imposed.pdf

# 4 a6 pages per a4: (print with -o sides=two-sided-long-edge)
pdftops boken.pdf -|psbook -s8|psnup -Pa5 -pa5 -2|psresize -Pa5 -pa4|pstops -pa4 '2:1U(210mm,297mm),0'|psbook -s4|psnup -2|pstops -pa4 '2:0U(210mm,297mm),1'|ps2pdf  -sPAPERSIZE=a4 -dEmbedAllFonts=true -dOptimized=true ->boken_a6.pdf

# 8 a7 pages per a4: (print with -o sides=two-sided-short-edge)
pdftops boken.pdf -|psbook -s16|psnup -Pa5 -pa5 -2|psresize -Pa5 -pa4|pstops -pa4 '2:0U(210mm,297mm),1'|psbook -s8|psnup -2|pstops -pa4 '2:0,U1(210mm,297mm)'|psbook -s4|psnup -2|ps2pdf  -sPAPERSIZE=a4 -dEmbedAllFonts=true -dOptimized=true ->boken_a7.pdf

