#! /bin/bash

convert book_front.png book_front.ps
convert book_front_B.png book_front_B.ps
convert book_back.png book_back.ps
convert book_back_B.png book_back_B.ps

pstopdf book_front.ps
pstopdf book_front_B.ps
pstopdf book_back.ps
pstopdf book_back_B.ps

rm *.ps
